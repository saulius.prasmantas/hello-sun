package main

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestRoot test main HandleFunc
func TestRoot(t *testing.T) {
	assert := assert.New(t)

	tests := []struct {
		description  string
		url          string
		expectedBody string
		expectedCode int
	}{
		{
			description:  "Main",
			url:          "/",
			expectedBody: "Hello Sun\n",
			expectedCode: 200,
		},
	}
	ts := httptest.NewRecorder()
	defer ts.Close()

	for _, tc := range tests {
		var u bytes.Buffer
		u.WriteString(string(ts.URL))
		u.WriteString(tc.url)

		res, err := http.Get(u.String())
		assert.NoError(err)
		if res != nil {
			defer res.Body.Close()
		}

		b, err := ioutil.ReadAll(res.Body)
		assert.NoError(err)

		assert.Equal(tc.expectedCode, res.StatusCode, tc.description)
		assert.Equal(tc.expectedBody, string(b), tc.description)
	}

}
