package main

import (
	"fmt"
	"log"
	"net/http"
)

func RootHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello Sun")
}
func main() {
	http.HandleFunc("/", RootHandler)
	log.Println("Listening on port :8000")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
